Dir["app/models/**/*.rb"].each do |model|
  load model
end
Dir["app/services/**/*.rb"].each do |service|
  load service
end

input = ARGV[0]

minute, hour, day_of_month, month, day_of_week, *command = input.split(' ')

printf("%-14s %s\n", "minute", Parser.new(minute, Minute.new).parse)
printf("%-14s %s\n", "hour", Parser.new(hour, Hour.new).parse)
printf("%-14s %s\n", "day of month", Parser.new(day_of_month, Day.new).parse)
printf("%-14s %s\n", "month", Parser.new(month, Month.new).parse)
printf("%-14s %s\n", "day of week", Parser.new(day_of_week, Weekday.new).parse)
printf("%-14s %s", "command", command.join(' ').to_s)
puts
