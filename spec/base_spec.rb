require 'spec_helper'
require_relative '../app/models/base'

describe "Base" do
  class TestBase < Base
    def min
      1
    end

    def max
      10
    end
  end

  subject { TestBase.new }

  context "#any" do
    it "returns all values" do
      expect(subject.any).to eq [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    end
  end

  context "#single_value" do
    context "when the value is out of bounds (lower)" do
      it "raises an exception" do
        expect { subject.single_value(0) }.to raise_exception("0 is out of bounds!")
      end
    end

    context "when the value is out of bounds (higher)" do
      it "raises an exception" do
        expect { subject.single_value(12) }.to raise_exception("12 is out of bounds!")
      end
    end

    context "when the value is OK" do
      it "returns the value" do
        expect(subject.single_value(8)).to eq [8]
      end
    end
  end

  context "#list" do
    context "when the list contains out of bounds values" do
      it "should raise an exception" do
        expect { subject.list([12]) }.to raise_exception("12 is out of bounds!")
      end
    end

    context "when the list is valid" do
      it "should return the list" do
        expect(subject.list([1,5,7])).to eq [1,5,7]
      end
    end
  end

  context "#range" do
    context "when the start value is out of bounds (lower)" do
      it "raises an exception" do
        expect { subject.range(0,8) }.to raise_exception("0 is out of bounds!")
      end
    end

    context "when the end value is out of bounds (higher)" do
      it "raises an exception" do
        expect { subject.range(5,12) }.to raise_exception("12 is out of bounds!")
      end
    end

    context "when the end value is smaller than start value" do
      it "raises an exception" do
        expect { subject.range(8,5) }.to raise_exception("Start value cannot be higher than the end value for range!")
      end
    end

    context "with loop around values" do
      it "returns the right values" do
        expect(subject.range(8,5)).to match_array([8,9,10,1,2,3,4,5])
      end
    end

    context "when the values is OK" do
      it "returns the value" do
        expect(subject.range(5,8)).to eq [5,6,7,8]
      end
    end
  end

  context "#stepped" do
    context "without a range as the start" do
      it "returns the right values" do
        expect(subject.stepped(2, 2, 10)).to eq [2, 4, 6, 8, 10]
      end
    end

    context "with a range as the start" do
      it "returns the right values" do
        expect(subject.stepped([1,2], 3, 10)).to eq [1, 2, 5, 8]
      end

      it "returns the right values" do
        expect(subject.stepped([1,2,1], 3, 10)).to eq [1, 2, 4, 7, 10]
      end
    end
  end
end
