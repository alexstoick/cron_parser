### cron_parser

## Installation

- Clone repo: `git clone git@bitbucket.org:alexstoick/cron_parser.git`
- Run bundle: `bundle install`

## Running the app

The app can be launched from the command line like so:
`ruby app.rb 'cron-string'`

It expects one parameter - a string which is the cron expression to be parsed.

## Specs

To run the specs:

`bundle exec rspec`

## Approach to solving the problem

The application features 6 _model_ classes for each section of the cron (`Minute`,
`Hour`, `Day`, `Month`, `Weekday`), which all define a **min** and **max** value for the
range they cover, eg: Minute is 0..59, Weekday is 0..6. All of these classes
inherit from the `Base` class which holds the logic for the different kinds of
ranges that can be passed to the objects: `single_value`, `range`, `stepped` and
`list`. There is validation in each of these functions to make sure that the
values supplied are not out of bounds.

The other class which holds logic is the `Parser` class - which is concerned
with splitting the string into the right portions and passing these values to
the right instance. The `Parser` class is also the place were we transform the
input into integers and can raise exceptions if the input is not valid.

## Areas for improvement

- The validation is a bit repetitive, could be all moved into one function to
prevent this
- The methods from the `Base` class could be moved into classes - making it
a bit easier to reason with them
- The input printing could be simplified - there's quite a bit of repetition
going on there

