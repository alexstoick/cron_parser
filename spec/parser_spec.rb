require 'spec_helper'
require_relative '../app/services/parser'
require_relative '../app/models/base'
require_relative '../app/models/minute'

describe "Parser" do
  subject { Parser.new(expression, instance) }
  let(:instance) { Minute.new }
  let(:fake_response) { [1,2] }

  context "with the any expression" do
    let(:expression) { "*" }
    it "correctly calls the any method" do
      allow(instance).to receive(:any).and_return(fake_response)
      subject.parse
      expect(instance).to have_received(:any)
    end
  end

  context "with a stepped expression" do
    let(:expression) { "3/10" }
    it "correctly calls the any method" do
      allow(instance).to receive(:stepped).and_return(fake_response)
      subject.parse
      expect(instance).to have_received(:stepped).with(3, 10, 59)
    end

    context "with a range as the start" do
      let(:expression) { "1,2,1/10" }

      it "correctly calls the method" do
        allow(instance).to receive(:stepped).and_return(fake_response)
        subject.parse
        expect(instance).to have_received(:stepped).with([1,2,1], 10, 59)
      end
    end
  end

  context "with a range expression" do
    let(:expression) { "10-15" }
    it "correctly calls the any method" do
      allow(instance).to receive(:range).and_return(fake_response)
      subject.parse
      expect(instance).to have_received(:range).with(10, 15)
    end
  end

  context "with a list expression" do
    let(:expression) { "10,15,12" }
    it "correctly calls the any method" do
      allow(instance).to receive(:list).and_return(fake_response)
      subject.parse
      expect(instance).to have_received(:list).with([10, 15, 12])
    end
  end

  context "with a single number" do
    let(:expression) { "10" }
    it "correctly calls the any method" do
      allow(instance).to receive(:single_value).and_return(fake_response)
      subject.parse
      expect(instance).to have_received(:single_value).with(10)
    end
  end

end
