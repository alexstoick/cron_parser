class Parser
  def initialize(expression, instance)
    @expression = expression
    @instance = instance
  end

  def parse
    if expression == "*"
      instance.any
    elsif expression.include?("/")

      if expression.split("/")[0] == "*"
        start_value = instance.min
      else
        initial_expression = expression.split("/")[0]
        if initial_expression.include?("-")
          start_value = make_int(initial_expression.split("-")[0])
          end_value = make_int(initial_expression.split("-")[1])
        elsif initial_expression.include?(",")
          start_value = initial_expression.split(",").map { |str| make_int(str) }
        else
          start_value = make_int(initial_expression) # 3
        end
      end

      end_value ||= instance.max
      step = make_int(expression.split("/")[1])

      instance.stepped(start_value, step, end_value)
    elsif expression.include?(",")
      instance.list(
        expression.split(",").collect { |str| make_int(str) }
      )
    elsif expression.include?("-")
      start_value = make_int(expression.split(/-/)[0])
      end_value = make_int(expression.split(/-/)[1])

      instance.range(start_value, end_value)
    else
      instance.single_value(make_int(expression))
    end.join(",")
  end

  private
  attr_reader :expression, :instance

  def make_int(str)
    Integer(str)
  rescue
    raise "Input Data Invalid - #{str} is not a number"
  end
end
