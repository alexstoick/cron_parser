class Base
  def any
    (min..max).to_a
  end

  def stepped(start_value, step, end_value)
    start_arr = Array(start_value)
    start_value = start_arr.pop

    start_values = start_arr.each { |value| check_in_bound(value) }

    check_in_bound(start_value)
    check_in_bound(end_value)
    check_in_bound(step)

    arr = []
    current_value = start_value
    while current_value <= end_value
      arr << current_value
      current_value += step
    end
    (start_values + arr).uniq.sort
  end

  def list(values)
    values.each { |value| check_in_bound(value) }
  end

  def range(start_value, end_value)
    if start_value < min || max < start_value
      raise "#{start_value} is out of bounds!"
    end

    if end_value < min || max < end_value
      raise "#{end_value} is out of bounds!"
    end


    if end_value < start_value
      ((start_value..max).to_a + (min..end_value).to_a).sort
    else
      (start_value..end_value).to_a
    end
  end

  def single_value(value)
    if value < min || max < value
      raise "#{value} is out of bounds!"
    end
    [value]
  end

  private

  def check_in_bound(value)
    if value < min || max < value
      raise "#{value} is out of bounds!"
    end
  end
end
